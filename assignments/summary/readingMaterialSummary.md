# IoT Basics  
**The Internet of things (IoT)** — a system of interrelated computing devices, mechanical and digital machines provided with unique identifiers and the ability to transfer data over a network without requiring human-to-human or human-to-computer interaction with computerized sensing, scanning and monitoring capabilities
## Industrial revolution  
**Industry 1.0**: Mechanisation, Steam Power  
**Industry 2.0**: Mass Production, Assembly Line, Electrical energy  
**industry 3.0**: Automation, Computers & Electronics  
**Industry 4.0**: Cyber physical systems, Internet of Things, Network  
## Industry 3.0 Architecture
Sensors --> PLC's -->SCADA & ERP  
* Sensors send data to PLC's which collect all the data and send it to SCADA and ERP systems for storing the data
* This data is stored in Excels and CSV's  
![architecture of industry 3.0](/extras/architecture-3.0.png)  
**Communication Protocols**:  Modbus, CANopen, EtherCAT, PROFINET 
## Industry 4.0 Architecture  
**Industry 4.0 = Industry 3.0 + Internet**  
![industry 4.0 architecture](/extras/architecture-4.0.png)  
**Communication Protocols**:  MQTT.org, AMQP, OPC UA, CoAP RFC 7252, Websockets, HTTP, Restful API
## Industry 3.0 vs Industry 4.0 Structure  
![3.0 vs 4.0 structure](/extras/3.0-vs-4.0.png)  
## Why Industry 4.0? 
Devices connected to the internet sends data  
This data can be used for :
* Dashboards 
* Remote web SCADA
* Remote control configurstion of devices 
* Predictive maintenance 
* Real time event stream processing 
* Analytics with predictive models  
* Automated Device provisioning, Auto discovery
* Real time alerts & alarms  
## Upgrading to Industry 4.0   
**General concerns when upgrading to Industry 4.0**: Cost, Downtime, Reliability  
**Solution to these concerns**: Get data from Industry 3.0 devices/meters/sensors without changes to the original device.Send the data to the Cloud using Industry 4.0 devices.  
i.e.,**Convert Industry 3.0 protocols to Industry 4.0 protocols**  
**Conversion challenges**:  Expensive hardware, Lack of documentation, Properitary PLC protocols  
## So, How to convert?  
A library that helps get data from industry 3.0 devices and send them to industry 4.0 cloud  
Use the API to take data from PLCs and send it to clouds  
It runs on small embedded boards hence reducing cost  
![](/extras/3.0-to-4.0.png)  
## Tools to analyse available data  
* **IoT TSDB tools**: Store data in Time series databases (eg. Prometheus, InfluxDB)
* **IoT Dashboards**: View all data into beautiful dashboards (eg. Grafana, Thingsboard)
* **IoT Platforms**: Analyse data on these platforms (eg. AWS IoT, Google IoT, Azure IoT,Thingsboard)  
* **Get alerts**: Get alerts on data using Zaiper, Twilio  

 




 





